yumrepo { nginx:
	enabled  => 1,
	descr    => 'nginx repo',
	baseurl => 'http://nginx.org/packages/centos/7/$basearch/',
	gpgcheck => 0,
	before => [package['nginx'],service['nginx']],
}

package { 'nginx':
	ensure => installed,
	before => service['nginx'],
}

file {'copy nginx cfg':
	path => '/etc/nginx/conf.d/default.conf',
	ensure => present,
	content =>'
                server {
                listen 8888;
                server_name localhost;

                location / {
                proxy_pass http://127.0.0.1:8080/;
                 }
                  }',
	before => service['nginx'],
}

service { 'nginx':
	enable => true,
	ensure => running,
}

package { 'policycoreutils-python':
	ensure => installed,
}

exec { 'add 8888 port':
	path   => '/usr/bin:/usr/sbin:/bin',
	command => 'semanage port -a -t http_port_t  -p tcp 8888', 
}

package { 'tomcat':
        ensure => installed,
	before => service['tomcat'],
}

service { 'tomcat':
        enable => true,
        ensure => running,
}

exec { 'allow proxy':
	path   => '/usr/bin:/usr/sbin:/bin',
	command => 'setsebool -P httpd_can_network_connect 1',
}


